function sk_ring_fire_init()
{
    initShaders(gl,SK_RAIN_FIRE_VERTEX_SHADER,SK_RAIN_FIRE_FRAG_SHADER);
    var texData = initTextures(gl,"u_noiseTexture","resource/ring_fire_noise.png",0);
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('Failed to get the storage location of a_Position');
        return -1;
    }

    var u_resolution = gl.getUniformLocation(gl.program, "u_resolution");
    if (u_resolution < 0) {
        console.log('Failed to get the storage location of u_resolution');
        return -1;
    }

    var u_time = gl.getUniformLocation(gl.program, "u_time");
    if (u_time < 0) {
        console.log('Failed to get the storage location of u_time');
        return -1;
    }

    var verticesTexCoords = new Float32Array([
        -1.0,1.0,
        -1.0,-1.0,
        1.0,1.0,
        1.0,-1.0
    ]);
    var FSIZE = verticesTexCoords.BYTES_PER_ELEMENT;
    var vertexTexCoordBuffer = gl.createBuffer();
    if (!vertexTexCoordBuffer) 
    {
        console.log('Failed to create the buffer object');
        return -1;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, verticesTexCoords, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, FSIZE * 2, 0);
    gl.enableVertexAttribArray(a_Position);
    var time = 0;
    setUpdate(60,()=>{
        gl.clear(gl.COLOR_BUFFER_BIT);
        // 需要重新绘制
        gl.uniform1f(u_time, time+=0.01);
        gl.uniform2f(u_resolution, 640.0,640.0);
        gl.uniform1i(texData.u_Sampler, texData.texUnit); 
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    });
}