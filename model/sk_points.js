function sk_points_init()
{
    initShaders(gl,SK_POINTS_VERTEX_SHADER,SK_POINTS_FRAG_SHADER);
    var u_PointSize = gl.getUniformLocation(gl.program, "u_PointSize");
    if (u_PointSize < 0) {
        console.log('Failed to get the storage location of u_PointSize');
        return -1;
    }
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('Failed to get the storage location of a_Position');
        return -1;
    }

    var verticesTexCoords = new Float32Array([
      -0.5, 0.5, 0.0, 1.0,
      -0.5, -0.5, 0.0, 1.0,
      0.5, 0.5, 0.0, 1.0,
      0.5, -0.5, 0.0, 1.0,    
    ]);

    var FSIZE = verticesTexCoords.BYTES_PER_ELEMENT;

    var vertexTexCoordBuffer = gl.createBuffer();
    if (!vertexTexCoordBuffer) {
      console.log('Failed to create the buffer object');
      return -1;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, verticesTexCoords, gl.STATIC_DRAW);

    gl.vertexAttribPointer(a_Position, 4, gl.FLOAT, false, FSIZE * 4, 0);
    gl.enableVertexAttribArray(a_Position);

    gl.uniform1f(u_PointSize, 10);
    gl.clearColor(0.0,0.0,0.0,1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.POINTS, 0, 4);
}