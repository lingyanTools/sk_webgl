function sk_wanhuachi_init()
{
    initShaders(gl,SK_WANHUATONG_VERTEX_SHADER,SK_WANHUATONG_FRAG_SHADER);
    var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    if (a_Position < 0) {
        console.log('Failed to get the storage location of a_Position');
        return -1;
    }

    var u_R = gl.getUniformLocation(gl.program, "u_R");
    if (u_R < 0) {
        console.log('Failed to get the storage location of u_R');
        return -1;
    }

    var u_r = gl.getUniformLocation(gl.program, "u_r");
    if (u_r < 0) {
        console.log('Failed to get the storage location of u_r');
        return -1;
    }

    var u_k = gl.getUniformLocation(gl.program, "u_k");
    if (u_k < 0) {
        console.log('Failed to get the storage location of u_k');
        return -1;
    }

    var u_colorRotate = gl.getUniformLocation(gl.program, "u_colorRotate");
    if (u_colorRotate < 0) {
        console.log('Failed to get the storage location of u_colorRotate');
        return -1;
    }
    var verticesTexCoords = new Float32Array();
    var FSIZE = verticesTexCoords.BYTES_PER_ELEMENT;
    var degree = 0;
    var vertexTexCoordBuffer = null;
    var bufferSize = 0;
    var colorRotate = 0.0;
    setUpdate(60,()=>{
        gl.clear(gl.COLOR_BUFFER_BIT);
        // 需要重新绘制
        if(sk_wanhuatong_redraw)
        {
            // 获取大小圆半径的最大公约数
            var gcd = sk_math_gcd(Math.ceil(sk_wanhuatong_r*100),Math.ceil(sk_wanhuatong_R*100));
            // 获取小圆绕大圆转多少圈绘制完成
            var loops = sk_wanhuatong_r*100/gcd;
            // 重新设置顶点数据，顶点保存的是角度
            verticesTexCoords = new Float32Array(loops*360);
            for(var i = 0;i<verticesTexCoords.length;i++)
            {
                verticesTexCoords[i] = i;
            }
            vertexTexCoordBuffer = updateBuffer(vertexTexCoordBuffer,verticesTexCoords,bufferSize);
            gl.vertexAttribPointer(a_Position, 1, gl.FLOAT, false, FSIZE * 1, 0);
            gl.enableVertexAttribArray(a_Position);
            degree = 0;
            colorRotate = 0;
            sk_wanhuatong_redraw = false;
        }
        gl.uniform1f(u_R, sk_wanhuatong_R);
        gl.uniform1f(u_r, sk_wanhuatong_r);
        gl.uniform1f(u_k, sk_wanhuatong_dis);
        if(degree>=verticesTexCoords.length)
        {
            colorRotate+=0.0628;
        }
        else
        {
            degree +=36.0;
        }
        gl.uniform1f(u_colorRotate, colorRotate);
        gl.drawArrays(gl.LINE_STRIP, 0, degree);
    });
}

function updateBuffer(vertexTexCoordBuffer, verticesTexCoords, size)
{
    if(size<verticesTexCoords.length)
    {
        if(gl.isBuffer(vertexTexCoordBuffer))
        {
            gl.deleteBuffer(vertexTexCoordBuffer);
        }
        var vertexTexCoordBuffer = gl.createBuffer();
        if (!vertexTexCoordBuffer) 
        {
            console.log('Failed to create the buffer object');
            return -1;
        }
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, verticesTexCoords, gl.STATIC_DRAW);
        return vertexTexCoordBuffer;
    }
}

// 大圆的半径
var sk_wanhuatong_redraw = true;
var sk_wanhuatong_R = 1.0;
function sk_wanhuachi_RChange(event)
{
    sk_wanhuatong_R = Number(event.target.value);
    sk_wanhuatong_redraw = true;
}

// 小圆的半径
var sk_wanhuatong_r = 0.4;
function sk_wanhuachi_rChange(event)
{
    sk_wanhuatong_r = Number(event.target.value);
    sk_wanhuatong_redraw = true;
}

// 画笔与小圆圆心的距离
var sk_wanhuatong_dis = 0.25;
function sk_wanhuachi_disChange(event)
{
    sk_wanhuatong_dis = Number(event.target.value);
    sk_wanhuatong_redraw = true;
}