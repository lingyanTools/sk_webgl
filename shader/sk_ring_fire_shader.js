var SK_RAIN_FIRE_VERTEX_SHADER=`
attribute vec2 a_Position;
void main() {
  gl_Position = vec4(a_Position.x,a_Position.y,0,1.0);
}`;

var SK_RAIN_FIRE_FRAG_SHADER=`
precision mediump float;
#define time u_time*0.15
#define tau 6.2831853
uniform vec2      u_resolution;		// viewport resolution (in pixels)
uniform float     u_time;       	// shader playback time (in seconds)
uniform sampler2D u_noiseTexture;  	// input channel. XX = 2D/Cube

mat2 makem2(in float theta)
{
  float c = cos(theta);
  float s = sin(theta);
  return mat2(c,-s,s,c);
}

float noise(in vec2 x)
{
	vec2 coord = x * 0.06;
	return texture2D(u_noiseTexture, coord - floor(coord)).x;
}

float fbm(in vec2 p)
{	
	float z=2.0;
	float rz = 0.0;
	vec2 bp = p;
	for (float i= 1.0;i < 18.0;i++)
	{
		rz+= abs((noise(p)-0.4)*2.0)/z;
		z = z*2.0;
		p = p*2.0;
	}
	return rz;
}

float dualfbm(in vec2 p)
{
  	//get two rotated fbm calls and displace the domain
	vec2 p2 = p*0.7;// p2的范围[-1.4,1.4]
	vec2 basis = vec2(fbm(p2-time*1.4),fbm(p2+time*1.7));
	basis = (basis-0.5)*0.76;
	p += basis;
	
	//coloring
	return fbm(p*makem2(time*0.72));
}

float circ(vec2 p) 
{
	float r = length(p);
	r = log(sqrt(r));
	return abs(mod(r*5.1,tau)-3.14)*3.0+0.2;
}

void main() {
	// 获取纹理坐标
	vec2 uv = gl_FragCoord.xy / u_resolution.xy;
	// 计算画布的宽高比
	float scale = u_resolution.x/u_resolution.y;
	// 计算片元在缓冲区的位置，以中心为原点，宽高都为1.0
	vec2 p = uv - 0.5;
	// 计算画布的宽高比，经过计算后的p宽高一致
	p.x *= scale;
	p*=4.0;	// p的范围为[-2.0,2.0]
	// 获取双重布朗运动
	float rz = dualfbm(p);
	//rings
	p /= exp(mod(3.0 * 10.0, 3.14159));
	rz *= pow(abs((0.1-circ(p))),0.9);
    
	uv.x *= scale;
  	vec2 center = vec2(0.5, 0.5);
  	center.x *= scale;
	  
  	float d = length(uv - center);
  	float r = 0.5;
  	float a = 1.0;
  	if(d > r) {
    	a = max(0.0, 1.0 - ((d - r) / 0.1));
  	}
	//final color
	vec3 col = vec3(0.321,0.1,0.065)/rz;
	col=pow(abs(col),vec3(0.99));
  	col *= a;
	gl_FragColor = vec4(col,1.0);
}`;