var SK_WANHUATONG_VERTEX_SHADER=`
attribute float a_Position;
uniform float u_R;
uniform float u_r;
uniform float u_k;
varying float v_radius;
void main() {
  // 根据大圆的角度计算万花尺画笔的位置
  float radius = radians(a_Position);
  float x1 = (u_R - u_r) * cos(radius);
  float y1 = (u_R - u_r) * sin(radius);
  float scale = -1.0 * u_R / u_r;
  float x2 = (u_r - u_k) * cos(scale * radius);
  float y2 = (u_r - u_k) * sin(scale * radius);
  float x = x1 + x2;
  float y = y1 + y2;
  gl_Position = vec4(x,y,0,1.0);
  v_radius = radius;
}`;

var SK_WANHUATONG_FRAG_SHADER=`
precision mediump float;
varying float v_radius;
uniform float u_colorRotate;
void main() {
  gl_FragColor = vec4(1.0,sin(v_radius+u_colorRotate),cos(v_radius+u_colorRotate),1.0);
}`;