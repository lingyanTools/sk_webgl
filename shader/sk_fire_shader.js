var PARTICLE_VERTEX_SHADER=`
attribute vec4 a_Position;
attribute vec2 a_TexCoord;
varying vec2 v_TexCoord;

void main() {
  gl_Position = a_Position;
  v_TexCoord = a_TexCoord;
}`;

var FRAG_SHADER=`
void main(){
    gl_FragColor=vec4(1.0,1.0,1.0,1.0);
}`;

var PARTICLE_FRAG_SHADER=`
precision mediump float;
uniform sampler2D u_Sampler;
uniform float offset;
uniform float scale;
uniform float time;
uniform int light;
uniform float color;
varying vec2 v_TexCoord;

float noise(vec3 p){
    vec3 i = floor(p);
    vec4 a = dot(i, vec3(1.0, 57.0, 21.0)) + vec4(0.0, 57.0, 21.0, 78.0);
    vec3 f = cos((p-i)*acos(-1.0))*(-0.5)+0.5;
    a = mix(sin(cos(a)*a),sin(cos(1.0+a)*(1.+a)), f.x);
    a.xy = mix(a.xz, a.yw, f.y);
    return mix(a.x, a.y, f.z);
}

float flame(vec3 p){
    vec3 orgOff = vec3(0.0,-1.0,0.0)- p*vec3(1.0,0.5,1.0);
    float d = length(p)-3.0;
    return d + (noise(p+vec3(time*0.5,time*0.5,time*0.5)) + noise(p*5.0)*0.5)*0.25*(p.y);
}

vec4 raymarch(vec3 org, vec3 dir){
    float d = 0.0, glow = 0.0, eps = 0.001;
    bool glowed = false;
    for(int i=0; i<1000; i++)
    {
        if(i>=light)
        {
            break;
        }
        float flameVal = flame(org);
        d = min(color-length(org) , abs(flameVal)) + eps;
        org += d * dir;
        if(d > eps)
        {
            // 在火焰照射范围内才发光
            if(flameVal < 0.0)
            {
                glowed=true;
            }
            if(glowed)
            {
                glow = float(i)/float(light);
            }    
        }
    }
    return vec4(org,glow);
}

void main() {
    vec2 v = offset+scale*v_TexCoord;
   
    vec3 org = vec3(0.0, 0.0, -4.0);
    vec3 dir = normalize(vec3(v.x, -v.y, 1.0));
   
    vec4 p = raymarch(org, dir);
    float glow = p.w;
    // 橙色(1.0,0.5,0.1,1.0)
    vec4 col = mix(vec4(1.0,0.5,0.1,1.0), vec4(1.0,0.5,0.1,1.0), p.y*0.02+0.4);
    //vec4 col = mix(vec4(1.0,0.5,0.1,0.0), texture2D(u_Sampler,v_TexCoord), p.y*0.02+0.4);
    //gl_FragColor = mix(vec4(0.0), col, pow(glow*2.0,4.0));    
    //gl_FragColor = mix(vec4(0.0,0.0,0.0,0.0), col, pow(glow*2.0,10.0));
    //gl_FragColor = vec4(pow(glow*2.0,10.0),pow(glow*2.0,10.0),pow(glow*2.0,10.0),1);
    gl_FragColor = vec4(glow,glow*0.5,glow*0.1,1.0);
}`;

var FIRE_FRAG_SHADER = `
precision highp float;
varying vec2 v_TexCoord;
uniform float time;

// 获取排列表中的值
float hash(vec2 p)  // replace this by something better
{
    p = 50.0*fract( p*0.3183099 + vec2(0.71,0.113));
    return -1.0+2.0*fract( p.x*p.y*(p.x+p.y) );
}

float noise_value(vec2 p)
{
    vec2 pi = floor(p);
    vec2 pf = fract(p);

    vec2 w = pf * pf * (3.0 - 2.0 * pf);

    return mix(
                mix(hash(pi + vec2(0.0, 0.0)), hash(pi + vec2(1.0, 0.0)), w.x),
                mix(hash(pi + vec2(0.0, 1.0)), hash(pi + vec2(1.0, 1.0)), w.x),
                w.y
            );
}

// 获取梯度值
vec2 grad( vec2 p)  // replace this anything that returns a random vector
{
    float hashV = hash(p);
    // simple random vectors
    return vec2(cos(hashV),sin(hashV));
}

float noise_perlin(vec2 p)
{
    vec2 i = floor( p );
    vec2 f = fract( p );

    vec2 u = f*f*(3.0-2.0*f); // feel free to replace by a quintic smoothstep instead

    return mix(mix( dot( grad( i+vec2(0,0) ), f-vec2(0.0,0.0) ), 
                     dot( grad( i+vec2(1,0) ), f-vec2(1.0,0.0) ), u.x),
                mix( dot( grad( i+vec2(0,1) ), f-vec2(0.0,1.0) ), 
                     dot( grad( i+vec2(1,1) ), f-vec2(1.0,1.0) ), u.x), u.y);
}

float fbm(vec2 uv)
{
    float f=0.0;
    uv=uv*8.0;
    mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
    f += 0.5000*noise_perlin( uv ); uv = m*uv;
    f += 0.2500*noise_perlin( uv ); uv = m*uv;
    f += 0.1250*noise_perlin( uv ); uv = m*uv;
    f += 0.0625*noise_perlin( uv ); uv = m*uv;
    f = 0.5 + 0.5 * f;
    f = smoothstep( 0.0, 1.0, f );
    return f;
}

void main() {
    vec2 uv = v_TexCoord;
    float flame = fbm(vec2(uv.x, uv.y-time/32.0));
    float c = 1.0 - 16.0 * pow(flame * uv.y, 2.0);
    float c1 = c * (1.0 - pow(uv.y, 4.0));
    c1=clamp(c1,0.,1.);
    vec3 col = vec3(1.5*c1, 1.5*c1*c1*c1, c1*c1*c1*c1*c1*c1);
    // vec4 color = vec4(0.1,.5,1.,1.0);
    gl_FragColor = vec4(col, c1);
    // gl_FragColor = vec4(flame);
}`;

var WHITE_NOISE_FRAG_SHADER=`
precision highp float;
varying vec2 v_TexCoord;
uniform float time;
uniform float u_midR;
uniform float u_rangeR;
uniform float u_strength;
vec2 grad(vec2 p)  // replace this anything that returns a random vector
{
    vec2 vec = vec2(p.x * 127.1 + p.y * 311.7, p.x * 269.5 + p.y * 183.3);
    float sin0 = sin(vec.x) * 43758.5453123;
    float sin1 = sin(vec.y) * 43758.5453123;
    vec.x = (sin0 - floor(sin0)) * 2.0 - 1.0;
    vec.y = (sin1 - floor(sin1)) * 2.0 - 1.0;
    // 归一化，尽量消除正方形的方向性偏差
    float len = sqrt(vec.x * vec.x + vec.y * vec.y);
    vec.x /= len;
    vec.y /= len;
    return vec;
}

float noise_perlin(vec2 p)
{
    // 获取晶格对应的四个点
    vec2 p0 = floor(p);             // 左下角的点
    vec2 p1 = p0 + vec2(1.0,0.0);   // 右下角的点
    vec2 p2 = p0 + vec2(0.0,1.0);   // 左上角的点
    vec2 p3 = p0 + vec2(1.0,1.0);   // 右上角的点

    // 获取四个点的梯度，其实就是类似白噪声的随机值
    vec2 g0 = grad(p0);         // 获取左下角的梯度
    vec2 g1 = grad(p1);         // 获取右下角的梯度
    vec2 g2 = grad(p2);         // 获取左上角的梯度
    vec2 g3 = grad(p3);         // 获取右上角的梯度

    // 获取四个点的方向向量，决定了梯度的权重
    vec2 f = fract(p);
    vec2 v0 = f-vec2(0.0,0.0);  // 获取左下角的方向向量
    vec2 v1 = f-vec2(1.0,0.0);  // 获取右下角的方向向量
    vec2 v2 = f-vec2(0.0,1.0);  // 获取左上角的方向向量
    vec2 v3 = f-vec2(1.0,1.0);  // 获取右上角的方向向量

    // 获取插值所需要的梯度
    float dot0 = dot(g0,v0);     // 获取左下角梯度在方向向量上的权值
    float dot1 = dot(g1,v1);     // 获取右下角梯度在方向向量上的权值
    float dot2 = dot(g2,v2);     // 获取左上角梯度在方向向量上的权值
    float dot3 = dot(g3,v3);     // 获取右上角梯度在方向向量上的权值

    // 计算混合因子，可以使用更高阶的指数因子
    vec2 u = f*f*(3.0-2.0*f);
    //vec2 u = 6.0 * pow(f, 5.0) - 15.0 * pow(f, 4.0) + 10.0 * pow(f, 3.0);

    // 进行插值，对于二维柏林噪声，插值的顺序为
    // 1. dot0与dot1 => dot01   //混合因子为u.x
    // 2. dot2与dot3 => dot23   //混合因子为u.x
    // 3. dot01与dot23          //混合因子为u.y
    float dot01 = mix(dot0,dot1,u.x);
    float dot23 = mix(dot2,dot3,u.x);
    return mix(dot01,dot23,u.y);
}
float fbm(vec2 uv)
{
    float f=0.0;
    uv=uv*u_strength;
    mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
    f += 0.5000*noise_perlin( uv ); uv = m*uv;
    f += 0.2500*noise_perlin( uv ); uv = m*uv;
    f += 0.1250*noise_perlin( uv ); uv = m*uv;
    f += 0.0625*noise_perlin( uv ); uv = m*uv;
    f = 0.5 + 0.5 * f;
    f = smoothstep( 0.0, 1.0, f );
    return f;
}

float cal(vec2 coord,vec2 offset,float weight)
{
    float flame = fbm(coord+offset);
    float c = 1.0 - 16.0 * pow(flame * weight,2.0);
    float c1 = c * (1.0 - pow(weight, 4.0));
    c1=clamp(c1,0.0,1.0);
    return c1;
}

/*
void main(){
    vec2 uv = v_TexCoord.xy;
    float opacity = v_TexCoord.z;
    uv = -1.0+uv*2.0;
    float flame = 0.0;
    float opa = 10.0;       // opa越小 越亮
    float len = length(uv);
    if(abs(len-u_midR)<u_rangeR)
    {
        // 获取经过柏林噪声和布朗运动后的值
        flame = fbm(vec2(uv.x, uv.y-time/16.0));
        opa = pow(abs(len-u_midR)/u_rangeR,0.7);
        // 根据uv坐标获取当前像素的角度,以垂直向上为0度
        float deg =  degrees(atan(uv.y,uv.x)-1.57);
        // 获取左右的透明度渐变
        //float lrOpa = clamp((abs(mod(deg,18.0)-9.0)/9.0)-0.2,0.0,1.0);
        //opa = max(opa,lrOpa);
    }
    opa = max(abs(uv.x), abs(uv.y));
    flame = fbm(vec2(uv.x, uv.y-time/4.0))*cos(uv.y);
    opa = pow(opa,1.0);
    float c = 1.0 - 12.0 * pow(flame * opa, 3.0);
    float c1 = c * (1.0 - pow(opa, 2.0));
    c1=clamp(c1,0.0,1.0);
    vec3 col = vec3(1.5*c1, 1.5*c1*c1, c1*c1*c1);
    gl_FragColor = vec4(col*opacity, c1*opacity);
}
*/
void main(){
    vec2 uv = v_TexCoord;
    uv = -1.0+uv*2.0;
    float opa = 0.0; 
    float len = length(uv);
    float c1 = 0.0;
    if(len<1.0)
    {
        opa = fbm(vec2(uv.x, uv.y-time/5.0));
        //opa = 1.0;
        c1 = pow(opa,0.5)*(1.0-len);
        if(abs(len-u_midR)<u_rangeR)
        {
            float a =1.0 - abs(len-u_midR)/u_rangeR;
            c1= pow(a,1.0)+c1;
        }
    }
    vec4 c = vec4(112.0/255.0*1.0, 211.0/255.0*1.0,1.0*1.0,1.0);
    gl_FragColor = c * c1;
}
`;