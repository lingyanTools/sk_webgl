var SK_ANIMATION_VERTEX_SHADER=`
attribute vec4 a_Position;
uniform float u_PointSize;
void main() {
  gl_Position = a_Position;
  gl_PointSize = u_PointSize;
}`;

var SK_ANIMATION_FRAG_SHADER=`
void main() {
  gl_FragColor = vec4(1.0,0.0,0.0,1.0);
}`;