var gl;
function main()
{
    var canvas = document.getElementById("wglCanvas");
    gl = getWebGLContext(canvas);
    if(!gl)
    {
        console.log("获取webgl渲染上下文失败");
        return;
    }
    gl.clearColor(0.0,0.0,0.0,1.0);
    var title = document.getElementById("title");
    console.log(title.textContent);
    if(title.textContent == "sk_points")
    {
        sk_points_init();
    }
    else if(title.textContent == "sk_animation")
    {
        sk_animation_init();
    }
    else if(title.textContent == "sk_wanhuachi")
    {
        sk_wanhuachi_init();
    }
    else if(title.textContent == "sk_ring_fire")
    {
        sk_ring_fire_init();
    }
}