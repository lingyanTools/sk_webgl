/**
 * 使用辗转相除法计算两个整数的最大公约数
 */
function sk_math_gcd(val1,val2)
{
    var a = Math.max(val1,val2);
    var b = Math.min(val1,val2);
    var c = a % b;
    while(c!=0)
    {
        a = b;
        b = c;
        c = a % b;
    }
    return b;
}