function initShaders(gl, vshader, fshader) {
    var program = createProgram(gl, vshader, fshader);
    if (!program) {
      console.log('Failed to create program');
      return false;
    }
    gl.useProgram(program);
    gl.program = program;
    return true;
}

function createProgram(gl, vshader, fshader) {
    var vertexShader = loadShader(gl, gl.VERTEX_SHADER, vshader);
    var fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fshader);
    if (!vertexShader || !fragmentShader) {
      return null;
    }
    var program = gl.createProgram();
    if (!program) {
      return null;
    }
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    var linked = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!linked) {
      var error = gl.getProgramInfoLog(program);
      console.log('Failed to link program: ' + error);
      gl.deleteProgram(program);
      gl.deleteShader(fragmentShader);
      gl.deleteShader(vertexShader);
      return null;
    }
    return program;
  }

// gl: webgl上下文
// type: 着色器类型
// source: 着色器代码
function loadShader(gl, type, source) {
    var shader = gl.createShader(type);
    if (shader == null) {
      console.log('unable to create shader');
      return null;
    }
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    var compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!compiled) {
      var error = gl.getShaderInfoLog(shader);
      console.log('Failed to compile shader: ' + error);
      gl.deleteShader(shader);
      return null;
    }
    return shader;
  }

  function getWebGLContext(canvas) {
    var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];
    var context = null;
    for (var ii = 0; ii < names.length; ++ii) {
      try {
        context = canvas.getContext(names[ii]);
      } catch(e) {}
      if (context) {
        break;
      }
    }
    return context;
  }
  
  /**
   * 加载纹理
   * @param gl：webgl
   * @param shaderName：纹理在shader中的名字
   * @param imgPath ：纹理的本地路径，或者网络路径
   * @param texUnit ：纹理单元，支持0、1、2
   * @returns 
   */
  function initTextures(gl,shaderName,imgPath,texUnit) {
      imgPath = "http://127.0.0.1:5500/"+imgPath;
      var texture = gl.createTexture(); 
      if (!texture) {
        console.log('Failed to create the texture');
        return false;
      }
      var u_Sampler = gl.getUniformLocation(gl.program, shaderName);
      if (!u_Sampler) {
        console.log('Failed to get the storage location of u_Sampler');
        return false;
      }
      var image = new Image();
      if (!image) {
        console.log('Failed to create the image object');
        return false;
      }
      image.onload = function(){ loadTexture(gl,texture, u_Sampler, image, texUnit); };
      image.src = imgPath;
      image.crossOrigin="*";
      return {loc:u_Sampler,unit:texUnit};
    } 
    function loadTexture(gl, texture, u_Sampler, image, texUnit) {
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
      switch(texUnit)
      {
          case 0:
              gl.activeTexture(gl.TEXTURE0);
              break;
          case 1:
              gl.activeTexture(gl.TEXTURE1);
              break;
          case 2:
              gl.activeTexture(gl.TEXTURE2);
              break;
      }
      gl.bindTexture(gl.TEXTURE_2D, texture);   
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
      gl.uniform1i(u_Sampler, texUnit); 
    }

    // 所有动画回调的集合
    var animCallbacks=new Array();

    /**
     * 设置刷新
     * @param {*} fps 刷新率
     * @param {*} updateCallback 回调函数，处理每帧的逻辑 
     */
    function setUpdate(fps,updateCallback)
    {
      updateCallback.interval = 1000/fps;
      updateCallback.lastTime = undefined;
      animCallbacks.push(updateCallback);
      if(animCallbacks.length==1)
      {
        tick();
      }
    }
  
    /**
     * 时钟，用于确定是否需要刷新
     */
    function tick(timeStamp)
    {
      requestAnimationFrame(tick.bind(this));
      animCallbacks.forEach(callback => {
        if(!callback)
        {
          return;
        }
        if(callback.lastTime == undefined)
        {
          callback.lastTime = timeStamp;
        }
        if(timeStamp && timeStamp - callback.lastTime>=callback.interval)
        {
          callback.lastTime = timeStamp;
          callback();
        }
      });
    }